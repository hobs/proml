# Book Proposal

## Suggested Titles


1. **_Machine Learning for Everyone_**

2. **_Prosocial Machine Learning_**

3. **_Machine Learning for Humans_**

## Suggested Subtitles

-   Machines that learn how to help **all** of us, not just the
    billionaires.

-   Building machines that cooperate rather than compete.

-   Teaching machines to learn more deeply by valuing more than just
    dollars.

-   Giving machines a worthwhile objective, saving humanity from itself.

**Authors: Maria Dyshel and Hobson Lane**

## Bio:

In 2019 Maria Dyshel and Hobson Lane founded Tangible AI to help
social-impact businesses employ AI for the greater good. Before that, In
2015, Hobson and his friends figured out how to build prosocial machines
that can read human language, so they wrote *Natural Language Processing
in Action* to spread the idea of prosocial machine intelligence. In
2020, Maria helped Hobson and their friends revise Natural Language
Processing in Action, based on what they learned about the positive and
negative impact that bots can have on the world. Now they're getting
back to basics to help everyone harness the power of machine learning
for good. They revised and updated their vision for a world where
machines and humans cooperate to save us from ourselves. And they want
to show everyone how to make that a reality for themselves.

## Book concept description:

*Machine Learning for Everyone* is different from any other book about
machine learning. It was written to help you create a better world, not
by enriching a big tech corporation, but by teaching machines to value
other humans the way you do. Capitalism rewards those that build good
stuff. *Machine Learning for Everyone* will show you how to build better
machines that cooperate with each other and with humans. You will teach
machines to help people become better more successful humans rather than
exploiting people for the benefit of the bot's masters.

In each chapter of this book, you will learn a new skill and a new
application of machine learning in the real world. Each chapter will
start with a toy problem to get your feet wet and encourage you to play
with what you learn. And the last half of each chapter will show you a
real world problem that skill can help you solve. And not only will you
learn these valuable technical skills, you'll gain an even more valuable
big picture perspective on the long term impact of what you've built.
We'll show you the dirty little secret that no-one is talking about, how
machines are being trained to be short-sighted and greedy rather than
helpful and cooperative.

You'll soon see how this obsession with immediate rewards can skew a
bot's intelligence and ingrain anti-social behavior, not only in the
machines, but in the humans around them. In each chapter you'll see how
to revise the objective for those machines so they learn how to be
smarter, less near-sighted. You will learn how to teach your machines to
win not only at zero-sum games like chess and home-price prediction but
at positive sum games like building a thriving social network or beating
banks and insurance companies at their own game. This will help you
build smarter, more generally intelligent and cooperative machines that
win in the marketplace by building up a thriving community of machines,
users and corporations around them.

## Audience:

This book is both an introduction to machine learning for beginning
Python developers and a gamified textbook to be used in college courses
and as a reference for business leaders, investors, and thought leaders.

-   Beginning Python programmers who have heard of machine learning and
    data science and want to learn more.

-   Seasoned data scientists and machine learning engineers that want a
    fresh perspective that can help them step up their game.

-   Academics and college students who are interested in the" why"
    behind successful machine learning models.

-   Professionals who find that their machine learning models aren't
    helping them grow their businesses as much as they hoped.

-   Business and thought leaders that want to set a course for their
    enterprises that will lead to long term success for both themselves
    and their community.

## What You Will Learn:

-   Machine learning fbasics like Linear Regression, Logistic Regression, and Decision Trees.

-   Ensembling models to improve accuracy and robustness

-   Layering models with Deep Learning to do better feature extraction
    and build more intelligent models

-   Transfer Learning, Fine tuning

-   PyTorch, Scikit-Learn, and SpaCy, open source tools that implement
    state-of-the art models.

-   Tabular data cleaning and machine learning, Natural Language
    Processing, Recommendation Engine, Time Series, and Bioinformatics
    models

-   How to deploy your models as a REST API

-   Data privacy and federated learning

-   Powerful generalization and unsupervised learning techniques that
    can help your models learn from fewer examples, like humans
    (few-shot learning).

-   Where to find machine learning datasets and real-world problems to
    cut your teeth on.

-   How to apply machine learning to the social-impact business sector.

-   How to glean insights not just predictions from machine learning
    models

## Key Features:

-   Quizzes (questions) and exercises in each chapter

-   A web page where readers can play games to test their knowledge and obtain help on their journey

-   Working python scripts that implement state of the art machine
    > learning models for all the most popular applications

-   Build confidence and deepen your understanding of the math and
    > algorithms behind machine learning

-   Show you how the exponential growth in machine learning technology
    > is changing the world economy, and society

-   Give you the skills to build a better business with more positive
    > social impact and long term success

-   Show you how to build models that learn from private user data
    > without jeopardizing anyone's privacy

-   Get familiar with the most important machine learning tools and
    > concepts used in the real world

-   Show you how to build models that don't ignore or exploit any subset
    > of your market

-   Develop habits that ensure you employ best practices on all your
    > machine learning and social-impace projects

-   Learn how to automate much of your work and your personal life with
    > machine learning models and good software development habits

-   Help you and your business compete against Data Robot, Auto ML, and
    > meta machine learning services.

## Approach

This book takes a hands-on gamified approach to learning. Each chapter
will present a real world problem and dataset and show you how to tackle
it in a prosocial way. At each step of the way you will level up your
skill by modifying the code examples to answer questions and solve
problems on your own before we show you how we do it in the real world.
And you'll be given a chance to share your way with the community by
contributing your answers to a public GitLab repository. This will
unlock your access to deeper insights and tools you can use in your work
and your life.

[add 3 closely related/competitive books/courses/resources]{.ul}

## Competition

1. *Natural Language Processing in Action* (Manning) by Hobson Lane

2. *Introduction to Machine Learning* (Coursera) by Andrew Ng

3. *Data Science from Scratch* (O'Reilly) by Joel Grus

## Preface[^1]

From nurses to doctors, delivery drivers to airline pilots, nearly everyone can benefit from learning how to harness machine learning for good.
In this book we will take you from zero to hero on your journey to create machine learning models and even chatbots for your own secret mission to save the world.
Machines and AI are offloading more and more of your work.
If you stand by and watch, you may find yourself in the unemployment line.
But if you have the right stuff you can learn how to take advantage of this cognitive assistance to give you superpowers for good.
And not only can machines help you help others, they can give you financial stability and take control of your future.
It pays to understand how machines "think".

In this book you will learn the basic skills you need to understand and even play around with machine learning.
And soon you will be building models to help you track your personal fitness and health or even help you at your job.
The kinds of machine learning models you will learn about in this book can provide you with cognitive assistance you can use to compete in the modern world and support your family and community.

[^1]: `Briefly explains what this book is all about, how it came about, what is
its purpose, how it will attempt to achieve it, what the reader can
expect, what are its contents, and who can benefit the most from reading
it.`

### Ch 1: Why Machine Learning: How machine learning works (20 Pages)

We will provide a brief description of the current state of Machine
Learning technology and how it is changing the way we live and work.

### Ch 2: Tabular Data and Linear Regression (30 Pages)

Target variables, features, linear regression formula.

Toy problem: simulate a linear system (height, weight, location, gender
or sex (inluding trans and nonbinary)

Real world: model public health data (smoking, covid, cancer, diabetes)
to identify potential causes of disease and improve your personal health

### Ch 3: Feature Engineering and Logistic Regression Classification

### Ch : Decision Trees and Boosting

### Ch : Basics of Deep Learning
principles of neural networksm types of neural networks (FF, CNN, RNN), attention/transformers?  

### Ch : Natural Language Processing
tokenization, stemming, TF-IDF, word embedding

real world: hate speech?

### Ch : Computer Vision


### Ch : Choosing and evaluating your model

### Ch : From Algorithm to Application: deploying and productizing ML models 

### Ch. X: Principles of Prosocial AI 


