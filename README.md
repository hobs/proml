# proml

_Prosocial Machine Learning_ -- A community-developed beginners guide that helps everyone teach machines to help us rather than exploit us. #wearenotprofit #donoharm #smarterai #betterthanus



## Alternative Titles:

* _Machine Learning for **Everyone**_
* _AI for **Everyone**_
* _Prosocial AI_
* _Socially Responsible AI_
* _Socially Responsible Machine Learning_
* _Machine Learning for the Greater Good_
* _Better Machine Learning_
* _Mindful Machine Learning_
* _Do No Harm Machine Learning_




<!-- [![Build Status](https://travis-ci.com/hobson/proml.svg?branch=master)](https://travis-ci.com/hobson/proml) -->
[![PyPI version](https://img.shields.io/pypi/pyversions/proml.svg)](https://pypi.org/project/proml/)
[![License](https://img.shields.io/pypi/l/proml.svg)](https://pypi.python.org/pypi/proml/)
[![Coverage](https://gitlab.com/hobs/proml/badges/master/coverage.svg)](https://gitlab.com/hobs/proml/)
